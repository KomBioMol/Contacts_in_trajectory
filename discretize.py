import os
import numpy as np
import pickle
from optparse import OptionParser

# direct = '/mnt/storage1/WORK/msm_trajs/TRF1_telo/equil'  # directory in which the analysis is performed
direct = os.getcwd()


def try_mkdir(dirname):
    """
    attempts to create a directory, catches exception if needed
    :param dirname: dir to create
    :return:
    """
    try:
        os.mkdir(dirname)
    except OSError:
        print("the dir '{}' already exists, will overwrite/modify contents...".format(dirname))


def sorter(name):
    """
    intended to sort files by filename, we should have something
    more robust here instead (consistent naming conventions?)
    :param name:
    :return:
    """
    return int(name.split('_')[1]) * 1000 + float(name.split('_')[2].replace('.dat', ''))


def assign_states(kwd='contacts'):
    """
    Reads binary contact patterns (trajectories converted by read_traj.py)
    and assigns each contact pattern to a discrete state (denoted by
    an integer) based on wheter it has appeared previously

    Note: currently the naming convention assumes input files follow the convention
    contactsXYYYY, where X is the state number (e.g. 0-5) and YYYY the trajectory
    identifier (e.g. the initial RC value, like 1.50)

    The converted contact patterns are saved in files statesXYYYY, and
    forward and backward assignments are stored in .pck files for later use
    :return:
    """
    d = [x for x in os.listdir(direct) if kwd in x]
    d.sort(key=sorter)
    d = [direct + '/' + x for x in d]
    
    trajs = []
    for f in d:
        trajs.append([line.strip() for line in open(f)])
    
    discrete_states = dict()
    inv_discrete_states = dict()
    n_states = 0
    
    for x in range(len(trajs)):
        t = trajs[x]
        for state in t:
            if state not in discrete_states.keys():
                discrete_states[state] = n_states
                inv_discrete_states[n_states] = state
                n_states += 1
        np.savetxt(d[x].replace(kwd, 'states'), np.array([discrete_states[state] for state in t]), fmt='%d')
    
    with open(direct + "/dict.pck", 'wb') as wfile:
        pickle.dump(discrete_states, wfile)
    with open(direct + "/inv_dict.pck", 'wb') as wfile:
        pickle.dump(inv_discrete_states, wfile)


def assign_location(string_state, degeneracy):
    """
    assigns location along the periodic path (unit complex number)
    and from the path (linear, based on number of native contacts made)
    :param string_state: original, string-based contact pattern
    :param degeneracy: length of the sequence
    :return: complex number, corresponds to the location in the contact space
    """
    # TODO generalize for any degeneracy/cont length
    len_single = int(len(string_state)/degeneracy)
    locs = [len_single - string_state[len_single * x:len_single * (x + 1)].count('0') for x in range(degeneracy)]
    conts = sum(locs)
    s_complex = sum([np.exp((x / degeneracy) * 2*np.pi * 1j) * locs[x] for x in range(degeneracy)]) / float(conts)
    z_real = np.max([x / len_single for x in locs])
    if abs(s_complex) < 10 ** (-8) or z_real < 10 ** (-8):
        return 0.0
    else:
        return z_real * s_complex / np.abs(s_complex)


def get_locs(degeneracy):
    """
    Maps individual states to the complex number that indicates
    their location in the contact space (along/binding coordinates);
    saves these values in a pck file
    :return:
    """
    with open(direct + "/dict.pck", 'rb') as rfile:
        discretes = pickle.load(rfile)
    state_location = {}
    for state in discretes.keys():
        state_location[discretes[state]] = assign_location(state, degeneracy)
    with open(direct + "/locs.pck", 'wb') as wfile:
        pickle.dump(state_location, wfile)


def count_states():
    """
    Yields state counts, i.e. tells how many times a given state
    appeared in all simulations combined
    :return:
    """
    s = [direct + '/' + x for x in os.listdir(direct) if 'states' in x]
    states_list = np.concatenate([np.loadtxt(f) for f in s])
    uni, count = np.unique(states_list, return_counts=True)
    state_counts = dict(zip(uni, count))
    with open(direct + "/counts.pck", 'wb') as wfile:
        pickle.dump(state_counts, wfile)


def parse_options():
    parser = OptionParser(usage="%prog -p top_file -f struct_file -l lesion_name -r residues_to_mutate")
    parser.add_option("-f", dest="core_name", action="store", type="string", default='contacts',
                      help="unique name that distinguishes files containing contact data "
                           "from MD trajectories (default is 'contacts'")
    parser.add_option("-d", dest="degeneracy", action="store", type="int",
                      help="length of the periodic sequence (e.g. 6 for telomeric, 2 for polyA)")
    (opts, args) = parser.parse_args()
    return opts


if __name__ == "__main__":
    options = parse_options()
    assign_states(options.core_name)
    get_locs(options.degeneracy)
    count_states()
