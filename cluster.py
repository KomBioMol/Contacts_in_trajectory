from discretize import try_mkdir, sorter
import pickle
import os
import numpy as np
from multiprocessing import Pool
from optparse import OptionParser

# direct = '/mnt/storage1/WORK/msm_trajs/TRF1_telo/equil'  # directory in which the analysis is performed
direct = os.getcwd()


def cluster_state(counts_pck_threshold):
    """
    Clusters states based on Hammond distances; also saves
    cluster assignment (i.e. which initial states are mapped
    to which final states)
    :param counts_pck_threshold: a tuple consisting of path to counts.pck
                                 and the value of the threshold for clustering
                                 (all states with counts less than threshold
                                 will be reassigned to a larger neighbor)
    :return:
    """
    directory = counts_pck_threshold[0]
    threshold = counts_pck_threshold[1]
    counts_dict = pickle.load(open(directory + '/counts.pck', 'rb'))
    maxcount = np.max(list(counts_dict.values())) + 1
    clust_dict = {}
    inv_state_assign = pickle.load(open(directory + "/inv_dict.pck", 'rb'))  # map from int to orig string
    state_assign = pickle.load(open(directory + "/dict.pck", 'rb'))  # map from orig string to int
    target_states_str = set(inv_state_assign[key] for key in counts_dict.keys() if counts_dict[key] >= threshold)
    too_small_states_int = set(key for key in counts_dict.keys() if counts_dict[key] < threshold)
    states_processed = 0
    for state_int in counts_dict.keys():  # for each assigned state
        state_str = inv_state_assign[state_int]  # check the original string
        if state_int in too_small_states_int:
            # either assign a nearest neighbor:
            clust_dict[state_int] = state_assign[find_nearest(state_str, target_states_str, counts_dict, maxcount,
                                                              state_assign)]
        else:
            # or the state itself:
            clust_dict[state_int] = state_int
        states_processed += 1
        if states_processed % 1000 == 0:
            print("processed {} states".format(states_processed))
    with open(directory + "/cluster_assignments-atleast{}.pck".format(threshold), 'wb') as wfile:
        pickle.dump(clust_dict, wfile)


def find_nearest(state, target_states, counts_dict, maxcount, state_assign):
    """
    Finds the closest state (in terms of Hammond distances) for the microstate
    to be incorporated/clustered into; also sorts by number of state members
    and returns the highest populated
    :param state:
    :param target_states:
    :param counts_dict:
    :param maxcount:
    :param state_assign:
    :return:
    """
    return sorted(target_states, key=lambda x: hammond(x, state) - counts_dict[state_assign[state]] / maxcount)[0]


def hammond(string1, string2):
    """
    Returns the Hammond distance between two states
    :param string1:
    :param string2:
    :return:
    """
    n = len(string1)
    distance = 0
    for x in range(n):
        if string1[x] != string2[x]:
            distance += 1
    return distance


def get_trajs_as_states(threshold):
    """
    Maps trajectories to clustered states
    after clustering was performed
    :param threshold:
    :return:
    """
    for thr in threshold:
        with open(direct + "/cluster_assignments-atleast{}.pck".format(thr), 'rb') as wfile:
            clust_dict = pickle.load(wfile)
        d = [x for x in os.listdir(direct) if 'contacts' in x]
        d.sort(key=sorter)
        d = [direct + '/' + x for x in d]
        try_mkdir(direct + '/threshold_{}'.format(thr))
        for x in range(len(d)):
            traj = np.loadtxt(d[x].replace('contacts', 'states'))
            traj_clust = np.array([clust_dict[x] for x in traj])
            np.savetxt(d[x].replace('contacts', 'threshold_{}/clustered-states'.format(thr)),
                       traj_clust, fmt='%15d')
        
        
def parallel_cluster_state(directory, thresholds, nproc):
    """
    Wrapper to run clustering in parallel
    :param directory:
    :param thresholds:
    :param nproc:
    :return:
    """
    if nproc > 0:
        p = Pool(nproc)
    else:
        p = Pool()
    p.map(cluster_state, [(directory, t) for t in thresholds])


def parse_options():
    parser = OptionParser(usage="%prog -p top_file -f struct_file -l lesion_name -r residues_to_mutate")
    parser.add_option("-t", dest="thresh", action="store", type=str,
                      help="minimal number of microstate members that will result in the"
                           "microstate being merged with a larger one")
    parser.add_option("-n", dest="nproc", action="store", type=int, default=-1,
                      help="number of CPUs for clustering; default is use all")
    (opts, args) = parser.parse_args()
    return opts


if __name__ == "__main__":
    options = parse_options()
    thresholds = [int(x) for x in options.thresh.split()]
    parallel_cluster_state(direct, thresholds, options.nproc)
    get_trajs_as_states(thresholds)
