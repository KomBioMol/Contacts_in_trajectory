from optparse import OptionParser
from itertools import product
from multiprocessing import Pool
from functools import reduce
import pickle as pck
import numpy as np
import os

from pyemma.thermo import estimate_umbrella_sampling as estimate_us


class RenumStates:
    def __init__(self):
        self.states_dict = None
        self.inv_states_dict = None
    
    def fit(self, *dtrajs):
        all_dtrajs = reduce(lambda x, y: x+y, dtrajs) if len(dtrajs) > 1 else dtrajs[0]
        ordered_states = sorted(list(set([state for dtraj in all_dtrajs for state in dtraj])))
        self.states_dict = {x: y for x, y in zip(ordered_states, list(range(len(ordered_states))))}
        self.inv_states_dict = {self.states_dict[x]: x for x in self.states_dict.keys()}
    
    def transform(self, dtraj_numpy):
        """
        Transforms from arbitrary state numbering
        (e.g. 3-24-122-185-293) to a consecutive,
        0-based (0-1-2-3-4)
        :param dtraj_numpy: a numpy array to be converted
        :return: np.ndarray, numpy array of identical shape
        """
        new_dtraj = 0 * dtraj_numpy.reshape(-1)
        for n, state in enumerate(dtraj_numpy):
            new_dtraj[n] = self.states_dict[state]
        return new_dtraj.reshape(dtraj_numpy.shape)
    
    def inv_transform(self, dtraj_numpy):
        """
        Transforms from consecutive state numbering
        (0-1-2-3-4) to original (e.g. 3-24-122-185-293)
        :param dtraj_numpy: a numpy array to be converted
        :return: np.ndarray, numpy array of identical shape
        """
        new_dtraj = 0 * dtraj_numpy.reshape(-1)
        for n, state in enumerate(dtraj_numpy):
            new_dtraj[n] = self.inv_states_dict[state]
        return new_dtraj.reshape((-1, 1))
    

def get_memm(inp):
    lag, nperm, threshold = inp
    perms = [q for q in range(nperm)]
    bname = os.getcwd() + '/'
    us_centers = sorted([float(x) for x in os.listdir(bname + 'US')
                         if x[0] in '1234567890'])
    us_force_constants = [500/(4.184*0.596) for _ in [0, 1] for x in us_centers]
    us_trajs = [np.loadtxt(bname + 'US/{:.3f}/us{}_pullx.xvg'.format(c, p),
                           comments=['#', '@'], usecols=1).reshape((-1, 1))
                for p in perms for c in us_centers]
    md_trajs = [np.loadtxt(bname + 'equil/{:.3f}/us{}_pullx.xvg'.format(c, p),
                           comments=['#', '@'], usecols=1).reshape((-1, 1))
                for p in perms for c in us_centers]
    us_dtrajs = [np.loadtxt(bname + 'combined/discrete_trajs/threshold_{}/'
                                    'clustered-states_{}_{:.3f}_us.dat'.format(threshold, p, c)).astype(int)
                 for p in perms for c in us_centers]
    md_dtrajs = [np.loadtxt(bname + 'combined/discrete_trajs/threshold_{}/'
                                    'clustered-states_{}_{:.3f}_eq.dat'.format(threshold, p, c)).astype(int)
                 for p in perms for c in us_centers]
    renumberer = RenumStates()
    renumberer.fit(us_dtrajs, md_dtrajs)
    us_dtrajs = [renumberer.transform(x) for x in us_dtrajs]
    md_dtrajs = [renumberer.transform(x) for x in md_dtrajs]
    tram = estimate_us(us_trajs=us_trajs, us_dtrajs=us_dtrajs, us_centers=us_centers*len(perms),
                       md_trajs=md_trajs, md_dtrajs=md_dtrajs, us_force_constants=us_force_constants,
                       estimator='tram', lag=lag, maxiter=1000, init='mbar', init_maxiter=100)
    tram.save(bname + 'combined/discrete_trajs/threshold_{}/'
                      'model_lag{}.memm'.format(threshold, lag))
    pck.dump(renumberer, open('combined/discrete_trajs/threshold_{}/'
                              'renumbered_states_lag{}.pck'.format(threshold, lag), 'wb'))
    

def get_memm_parallel(lagtime, perms, threshold, nproc):
    lag = [int(x) for x in lagtime.split()]
    thr = [int(x) for x in threshold.split()]
    perms = [perms]
    stack = product(lag, perms, thr)
    p = Pool(nproc)
    p.map(get_memm, stack)
    
    
def parse_options():
    parser = OptionParser(usage="%prog -t threshold -n nperm -l lagtime")
    parser.add_option("-t", dest="threshold", action="store", type="str",
                      help="minimal population of microstate that is preserved "
                           "during clustering")
    parser.add_option("-l", dest="lagtime", action="store", type="str",
                      help="lagtime for the Markov state model")
    parser.add_option("-p", dest="nperm", action="store", type="int",
                      help="number of sequence permutations (in practice, either "
                           "2 for polyA or 6 for telomeric)")
    parser.add_option("-n", dest="nproc", action="store", type="int", default=1,
                      help="number of processes to run")
    (opts, args) = parser.parse_args()
    return opts


if __name__ == "__main__":
    options = parse_options()
    get_memm_parallel(options.lagtime, options.nperm, options.threshold, options.nproc)
