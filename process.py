import os
import numpy as np
import pickle
from multiprocessing import Pool

direct = '/mnt/storage1/WORK/msm_trajs/TRF1_telo/equil'  # directory in which the analysis is performed


def try_mkdir(dirname):
    """
    attempts to create a directory, catches exception if needed
    :param dirname: dir to create
    :return:
    """
    try:
        os.mkdir(dirname)
    except OSError:
        print("the dir '{}' already exists, will overwrite/modify contents...".format(dirname))


def sorter(name):
    """
    intended to sort files by filename, we should have something
    more robust here instead (consistent naming conventions?)
    :param name:
    :return:
    """
    return int(name[8]) * 1000 + float(name[9:])


def assign_states():
    """
    Reads binary contact patterns (trajectories converted by read_traj.py)
    and assigns each contact pattern to a discrete state (denoted by
    an integer) based on wheter it has appeared previously
    
    Note: currently the naming convention assumes input files follow the convention
    contactsXYYYY, where X is the state number (e.g. 0-5) and YYYY the trajectory
    identifier (e.g. the initial RC value, like 1.50)
    
    The converted contact patterns are saved in files statesXYYYY, and
    forward and backward assignments are stored in .pck files for later use
    :return:
    """
    d = [x for x in os.listdir(direct) if 'contacts' in x]
    d.sort(key=sorter)
    d = [direct + '/' + x for x in d]
    
    trajs = []
    for f in d:
        trajs.append([line for line in open(f)])
    
    discrete_states = dict()
    inv_discrete_states = dict()
    n_states = 0
    
    for x in range(len(trajs)):
        t = trajs[x]
        for state in t:
            if state not in discrete_states.keys():
                discrete_states[state] = n_states
                inv_discrete_states[n_states] = state
                n_states += 1
        np.savetxt(d[x].replace('contacts', 'states'), np.array([discrete_states[state] for state in t]), fmt='%d')
    
    with open(direct + "/dict.pck", 'wb') as wfile:
        pickle.dump(discrete_states, wfile)
    with open(direct + "/inv_dict.pck", 'wb') as wfile:
        pickle.dump(inv_discrete_states, wfile)


def assign_location(string_state):
    """
    assigns location along the periodic path (unit complex number)
    and from the path (linear, based on number of native contacts made)
    :param string_state: original, string-based contact pattern
    :return: complex number, corresponds to the location in the contact space
    """
    # TODO generalize for any degeneracy/cont length
    locs = [string_state[16*x:16*(x+1)].count('1') for x in range(6)]
    conts = sum(locs)
    s_complex = sum([np.exp((x/3.0)*np.pi*1j)*locs[x] for x in range(6)])/float(conts)
    z_real = np.max([x/16 for x in locs])
    if abs(s_complex) < 10**(-8) or z_real < 10**(-8):
        return 0.0
    else:
        return z_real * s_complex/np.abs(s_complex)

    
def get_locs():
    """
    Maps individual states to the complex number that indicates
    their location in the contact space (along/binding coordinates);
    saves these values in a pck file
    :return:
    """
    with open(direct + "/dict.pck", 'rb') as rfile:
        discretes = pickle.load(rfile)
    state_location = {}
    for state in discretes.keys():
        state_location[discretes[state]] = assign_location(state)
    with open(direct + "/locs.pck", 'wb') as wfile:
        pickle.dump(state_location, wfile)


def count_states():
    """
    Yields state counts, i.e. tells how many times a given state
    appeared in all simulations combined
    :return:
    """
    s = [direct + '/' + x for x in os.listdir(direct) if 'states' in x]
    states_list = np.concatenate([np.loadtxt(f) for f in s])
    uni, count = np.unique(states_list, return_counts=True)
    state_counts = dict(zip(uni, count))
    with open(direct + "/counts.pck", 'wb') as wfile:
        pickle.dump(state_counts, wfile)
    
    
def cluster_state(counts_pck_threshold):
    """
    Clusters states based on Hammond distances; also saves
    cluster assignment (i.e. which initial states are mapped
    to which final states)
    :param counts_pck_threshold: a tuple consisting of path to counts.pck
                                 and the value of the threshold for clustering
                                 (all states with counts less than threshold
                                 will be reassigned to a larger neighbor)
    :return:
    """
    counts_pck = counts_pck_threshold[0]
    threshold = counts_pck_threshold[1]
    counts_dict = pickle.load(open(counts_pck, 'rb'))
    maxcount = np.max(list(counts_dict.values())) + 1
    clust_dict = {}
    inv_state_assign = pickle.load(open(direct + "/inv_dict.pck", 'rb'))  # map from int to orig string
    state_assign = pickle.load(open(direct + "/dict.pck", 'rb'))  # map from orig string to int
    target_states_str = set(inv_state_assign[key] for key in counts_dict.keys() if counts_dict[key] >= threshold)
    too_small_states_int = set(key for key in counts_dict.keys() if counts_dict[key] < threshold)
    states_processed = 0
    for state_int in counts_dict.keys():  # for each assigned state
        state_str = inv_state_assign[state_int]  # check the original string
        if state_int in too_small_states_int:
            # either assign a nearest neighbor:
            clust_dict[state_int] = state_assign[find_nearest(state_str, target_states_str, counts_dict, maxcount,
                                                              state_assign)]
        else:
            # or the state itself:
            clust_dict[state_int] = state_int
        states_processed += 1
        if states_processed % 1000 == 0:
            print("processed {} states".format(states_processed))
    with open(direct + "/cluster_assignments-atleast{}.pck".format(threshold), 'wb') as wfile:
        pickle.dump(clust_dict, wfile)


def find_nearest(state, target_states, counts_dict, maxcount, state_assign):
    """
    Finds the closest state (in terms of Hammond distances) for the microstate
    to be incorporated/clustered into; also sorts by number of state members
    and returns the highest populated
    :param state:
    :param target_states:
    :param counts_dict:
    :param maxcount:
    :param state_assign:
    :return:
    """
    return sorted(target_states, key=lambda x: hammond(x, state) - counts_dict[state_assign[state]] / maxcount)[0]


def hammond(string1, string2):
    """
    Returns the Hammond distance between two states
    :param string1:
    :param string2:
    :return:
    """
    n = len(string1)
    distance = 0
    for x in range(n):
        if string1[x] != string2[x]:
            distance += 1
    return distance


def get_trajs_as_states(threshold):
    """
    Maps trajectories to clustered states
    after clustering was performed
    :param threshold:
    :return:
    """
    with open(direct + "/cluster_assignments-atleast{}.pck".format(threshold), 'rb') as wfile:
        clust_dict = pickle.load(wfile)
    d = [x for x in os.listdir(direct) if 'contacts' in x]
    d.sort(key=sorter)
    d = [direct + '/' + x for x in d]
    try_mkdir(direct + '/threshold_{}')
    for x in range(len(d)):
        traj = np.loadtxt(d[x].replace('contacts', 'states'))
        traj_clust = np.array([clust_dict[x] for x in traj])
        np.savetxt(d[x].replace('contacts', 'threshold_{}/clustered-states'), traj_clust, fmt='%15d')


def build_msm(threshold, lagtime):
    """
    Builds MSMs and ensures that eigendecomposition
    was performed in the model, then saves as .pck
    :param threshold:
    :param lagtime:
    :return:
    """
    from msmbuilder.msm import MarkovStateModel
    d = [x for x in os.listdir(direct) if 'contacts' in x]
    d.sort(key=sorter)
    d = [direct + '/' + x for x in d]
    trajs = []
    for x in range(len(d)):
        traj = np.loadtxt(d[x].replace('contacts', 'threshold_{}/clustered-states'))
        trajs.append(traj)
    msm = MarkovStateModel(lag_time=lagtime)
    msm.fit(trajs)
    _ = msm.timescales_  # ensures that we have eigenv's calculated
    with open(direct + "/msm_lag{}.pck".format(lagtime), 'wb') as wfile:
        pickle.dump(msm, wfile)
    

def get_implied(lagtime):
    """
    Writes implied timescales for the MSM
    :param lagtime:
    :return:
    """
    with open(direct + "/msm_lag{}.pck".format(lagtime), 'rb') as rfile:
        msm = pickle.load(rfile)
    with open(direct + "/msm_lag{}.tscales".format(lagtime), 'w') as wfile:
        wfile.write(" ".join([str(x) for x in msm.timescales_]))
    # with open(direct + "/msm_lag{}_tscales.pck".format(lagtime), 'wb') as wfile:
    #     pickle.dump(msm, wfile)


def cross_val(lag_thres, prop=0.75):
    """
    Performs cross-validation based on splitting the trajectory set
    into training and testing set with proportion given by prop
    :param lag_thres:
    :param prop:
    :return:
    """
    lagtime = lag_thres[0]
    threshold = lag_thres[1]
    from msmbuilder.msm import MarkovStateModel
    d = [x for x in os.listdir(direct) if 'contacts' in x]
    d.sort(key=sorter)
    d = [direct + '/' + x for x in d]
    d_train = list(np.random.choice(d, int(len(d)*prop), replace=False))
    d_test = [tr for tr in d if tr not in d_train]
    trajs_train = []
    for x in range(len(d_train)):
        traj = np.loadtxt(d_train[x].replace('contacts', 'threshold_{}/clustered-states'.format('threshold')))[::2]
        trajs_train.append(traj)
    msm = MarkovStateModel(lag_time=int(lagtime/2))
    msm.fit(trajs_train)
    score_train = msm.score(trajs_train)
    trajs_test = []
    for x in range(len(d_test)):
        traj = np.loadtxt(d_test[x].replace('contacts', 'threshold_{}/clustered-states'.format('threshold')))
        trajs_test.append(traj)
    score_test = msm.score(trajs_test)
    with open(direct + '/threshold_{}/scores_lag{}.dat'.format(threshold, lagtime), 'a') as logfile:
        logfile.write("train GMRQ score: {}\n".format(score_train))
        logfile.write("test GMRQ score: {}\n".format(score_test))


def parallel_cv(lagtimes, threshold, nproc=4):
    """
    Wrapper to run cross-validation in parallel
    :param lagtimes:
    :param threshold:
    :param nproc:
    :return:
    """
    p = Pool(nproc)
    p.map(cross_val, [(l, threshold) for l in lagtimes])
    

def parallel_cluster_state(directory, thresholds, nproc=4):
    """
    Wrapper to run clustering in parallel
    :param directory:
    :param thresholds:
    :param nproc:
    :return:
    """
    p = Pool(nproc)
    p.map(cluster_state, [(directory, t) for t in thresholds])


def parallel_get_trajs_as_states(thresholds, nproc=4):
    """
    Wrapper to run trajectory mapping in parallel
    :param thresholds:
    :param nproc:
    :return:
    """
    p = Pool(nproc)
    p.map(get_trajs_as_states, thresholds)
    
    
def gen_traj(msmfile, length=10000000, ntraj=10):
    """
    Generates MSM-based random trajectories
    of specified length
    :param msmfile:
    :param length:
    :param ntraj:
    :return:
    """
    with open(direct + "/locs.pck", 'rb') as locsfile:
        locs = pickle.load(locsfile)
    with open(msmfile, 'rb') as msmfile:
        msm = pickle.load(msmfile)
    for i in range(ntraj):
        traj = msm.sample(None, length)
        traj_complex = np.array([locs[x] for x in traj])
        with open("traj{}.dat".format(i), 'w') as wfile:
            for frame in traj_complex:
                wfile.write("{:10.4f} {:10.4f}\n".format(*frame))


if __name__ == "__main__":
    # assign_states()
    get_locs()
    gen_traj(direct + '/msm_lag12500.pck')
    # count_states()
    # thresholds = [335, 260, 219, 187, 148, 122, 105, 81, 66, 56, 48]
    # parallel_cluster_state(direct + "/counts.pck", thresholds)
    # parallel_get_trajs_as_states(thresholds)
    # for thresh in thresholds:
    #     parallel_cv([2, 10, 20, 100, 200, 1000, 2000, 10000], thresh)
        #for i in [1, 10]: # , 50, 100, 500, 1000, 5000, 10000]:
            #build_msm(i)
            #get_implied(i)
