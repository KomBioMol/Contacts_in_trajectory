import mdtraj as md
import numpy as np
from itertools import product
from optparse import OptionParser

"""
Script automates generation of protein-DNA residue pairs for subsequent contact analysis;
returns pairs that are in contact during at least half of the trajectory, as well as all permutations
with respect to DNA sequence
"""


def read_compute(opts):
    traj = md.load_xtc(opts.traj, top=opts.pdb)
    prot = [i.index for i in traj.atom_slice(traj.top.select('protein')).top.residues]
    dna = [i.index for i in traj.atom_slice(traj.top.select('resname DA DC DG DT PT3 PT5 PA3 PA5 PC3 PC5 PG3 PG5')).top.residues]
    dists, pairs = md.compute_contacts(traj, contacts=np.array(list(product(prot, dna))), scheme='closest-heavy')
    chosen_pairs = pairs[np.where(np.array([len(np.where(dists[:, i] < 0.4)[0])
                                            for i in range(len(dists[0]))]) > (len(traj)/2))[0]]
    chosen_pairs = np.sort(chosen_pairs, axis=1)
    if opts.switch:
        chosen_pairs = chosen_pairs[:, ::-1]
    return prot, dna, chosen_pairs


def choose_ds_dna(chosen_pairs, dna, prot, opts):
    """
    provides a selection for subsequent contact calculation
    in double-stranded DNA
    :param chosen_pairs: np.ndarray, array of pairs that define the native contacts
    :param dna: iterable, list of DNA residues
    :param prot: iterable, list of protein residues
    :param len_rept_seq: int, length of the repeating sequence defined for analysis (2 for polyA and 6 for telomeric)
    :return: list, all pairs for which contacts should be computed
    """
    len_rept_seq = opts.repeating
    all_chosen = []
    strand_length = int(len(dna)/2)
    offset_prot = len(prot) if not opts.switch else 0
    for j in range(len_rept_seq):
        for p in chosen_pairs:
            if p[1] in dna[:strand_length]:
                for i in range(int(strand_length/len_rept_seq)):
                    all_chosen.append([p[0], np.mod(((p[1]-len_rept_seq*i-j)-offset_prot), len(dna))+offset_prot])
            else:
                for i in range(int(strand_length/len_rept_seq)):
                    all_chosen.append([p[0], np.mod(((p[1]+len_rept_seq*i+j)-offset_prot), len(dna))+offset_prot])
    return all_chosen
                    
                    
def choose_ss_dna(chosen_pairs, dna, prot, opts):
    """
    As above, but for single-stranded DNA
    """
    len_rept_seq = opts.repeating
    all_chosen = []
    strand_length = len(dna)
    offset_prot = len(prot) if not opts.switch else 0
    for j in range(len_rept_seq):
        for p in chosen_pairs:
            for i in range(int(strand_length/len_rept_seq)):
                all_chosen.append([p[0], np.mod(((p[1]-len_rept_seq*i-j)-offset_prot), len(dna))+offset_prot])
    return all_chosen


def write_to_file(filename, contacts):
    with open(filename, 'w') as outfile:
        for p in contacts:
            outfile.write("{} {}\n".format(*p))


def parse_options():
    parser = OptionParser(usage="%prog -p top_file -f struct_file -l lesion_name -r residues_to_mutate")
    parser.add_option("-f", dest="traj", action="store", type="string",
                      help="an XTC trajectory to be analyzed")
    parser.add_option("-s", dest="pdb", action="store", type="string",
                      help="structure file compatible with the trajectory (.pdb or .gro)")
    parser.add_option("-o", dest="outputfile", action="store", type="string", default='contacts.dat',
                      help="name of the output file, default is contacts.dat")
    parser.add_option("-d", dest="dna", action="store", type="string",
                      help="type of DNA (either ss for single-stranded or ds for double-stranded)")
    parser.add_option("-n", dest="repeating", action="store", type='int',
                      help="length of the repeating sequence for analysis (e.g. 2 for polyA and 6 for telomeric)")
    parser.add_option("-x", dest='switch', action='store_true',
                      help='use as flag if DNA precedes protein in the structure')
    (opts, args) = parser.parse_args()
    return opts


if __name__ == "__main__":
    options = parse_options()
    protein, nucleic, chosen = read_compute(options)
    if options.dna == "ss":
        all_chosen_contacts = choose_ss_dna(chosen, nucleic, protein, options)
    elif options.dna == "ds":
        all_chosen_contacts = choose_ds_dna(chosen, nucleic, protein, options)
    else:
        raise ValueError('-d option can only be "ds" or "ss"')
    write_to_file(options.outputfile, all_chosen_contacts)
