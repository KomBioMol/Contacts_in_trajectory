import numpy as np
import mdtraj as md
from optparse import OptionParser

"""
Converts a trajectory to a contact pattern
based on mapping provided by gen_init_contacts.py
"""


def process(opts):
    xtc_in = opts.traj
    pdb_in = opts.structfile
    txt_out = opts.outputfile
    t = md.load_xtc(xtc_in, pdb_in)
    con = np.loadtxt(opts.pairs)
    distances = md.compute_contacts(t, contacts=con, scheme='closest-heavy', ignore_nonprotein=True)[0]
    contacts = (distances < 0.4)+0
    num = opts.degenerate
    result = np.zeros([contacts.shape[0], int(contacts.shape[1]/num)])
    for j in range(distances.shape[0]):
        result[j] = tuple(sum(contacts[j][num*i:num*(i+1)]) for i in range(int(distances.shape[1]/num)))
    if opts.permute:
        unit = int(len(result[0])/opts.perlen)
        assert opts.perlen * unit == len(result[0])
        from_index = len(result[0]) - (opts.permute * unit)
        for n in range(len(result)):
            result[n] = np.concatenate([result[n][from_index:], result[n][:from_index]])
    np.savetxt(txt_out, result, fmt='%1d', delimiter='')


def parse_options():
    parser = OptionParser(usage="%prog -p top_file -f struct_file -l lesion_name -r residues_to_mutate")
    parser.add_option("-f", dest="traj", action="store", type="string",
                      help="an XTC trajectory to be analyzed")
    parser.add_option("-s", dest="structfile", action="store", type="string",
                      help="structure file compatible with the trajectory (.pdb or .gro)")
    parser.add_option("-o", dest="outputfile", action="store", type="string", default='discrete_traj.dat',
                      help="name of the output file, default is discrete_traj.dat")
    parser.add_option("-i", dest="pairs", action="store", type="string",
                      help="output from gen_init_contacts.py")
    parser.add_option("-n", dest="degenerate", action="store", type='int',
                      help="number of degenerate sites "
                           "(e.g. 5 (30/6) for telomeric dsDNA of 30 bp, "
                           "15 (30/2) for polyA dsDNA of 30 bp, "
                           "3 (18/6) for telomeric ssDNA of 18 bases, "
                           "9 (18/2) for polyA ssDNA of 18 bases)")
    parser.add_option("-p", dest="permute", action="store", type='int', default=0,
                      help="induce cyclic permutation into the sequence")
    parser.add_option("-l", dest="perlen", action="store", type='int', default=1,
                      help="length of the periodic sequence "
                           "e.g. 6 for poly(TTAGGG)")
    (opts, args) = parser.parse_args()
    return opts


if __name__ == "__main__":
    options = parse_options()
    process(options)
