import numpy as np
from msmbuilder.msm import MarkovStateModel
import pyemma
# import pickle as pck
#
# msm = pck.load(open('msm.pck', 'rb'))
# locs = pck.load(open('locs.pck', 'rb'))


def create_toy_msmb():
    model = MarkovStateModel(lag_time=1)
    traj = [0, 0, 0, 1, 0, 4, 5, 1, 4, 4, 5, 4, 1, 0]
    locs = {0:0, 1:1j, 4:-1, 5:-1j}
    model.fit([traj])
    return model, locs


model, locs = create_toy_msmb()

print(np.max(list(model.mapping_.keys())))


def print_dynamic_attr_msmb(model):
    print(model.countsmat_)
    print(model.mapping_)
    print(model.n_states_)
    print(model.populations_)
    print(model.transmat_)
    

def find_attrs_meths(object):
    import types
    attrs = []
    meths = []
    for x in dir(object):
        try:
            if not isinstance(getattr(object, x), types.MethodType):
                attrs.append(x)
            else:
                meths.append(x)
        except:
            pass
    return attrs, meths


def double_all_msmb(model):
    orig_nstates = model.n_states_
    orig_highest = np.max(list(model.mapping_.keys()))
    model.populations_ = 0.5*np.hstack((model.populations_, model.populations_))
    inv_mapping = {model.mapping_[x]:x for x in model.mapping_.keys()}
    orig_keys = list(model.mapping_.keys())
    for snum in orig_keys:
        model.mapping_[orig_highest + snum + 1] = model.mapping_[snum] + orig_nstates
    model.n_states_ = 2*model.n_states_
    model.countsmat_ = double_matrix(model.countsmat_, inv_mapping, locs)
    model.transmat_ = double_matrix(model.transmat_, inv_mapping, locs)
    return model


def double_all_emma(model):
    pass


def double_matrix(matrix, state_assignments, state_locs):
    matrix_shape = matrix.shape[0]
    new_matrix = np.zeros((2*matrix_shape, 2*matrix_shape))
    for i in range(matrix_shape):
        for j in range(matrix_shape):
            iloc = state_locs[state_assignments[i]]
            jloc = state_locs[state_assignments[j]]
            if ij_close(iloc, jloc):
                new_matrix[i,j] = matrix[i,j]
                new_matrix[matrix_shape+i, matrix_shape+j] = matrix[i, j]
            else:
                new_matrix[matrix_shape + i, j] = matrix[i, j]
                new_matrix[i, matrix_shape + j] = matrix[i, j]
    return new_matrix


def ij_close(iloc, jloc):
    iang = np.angle(iloc)
    jang = np.angle(jloc)
    if iang == jang:
        return True
    if iang * jang < 0 or iang == 0 or jang == 0:
        if iang > 0:
            q = -np.pi + iang
            if jang > q:
                return False
        else:
            q = -np.pi + jang
            if iang > q:
                return False
    return True

print_dynamic_attr_msmb(model)
model = double_all_msmb(model)
print_dynamic_attr_msmb(model)