import matplotlib.pyplot as plt
import pyemma
import pickle as pck
from estimate_emma import RenumStates
import numpy as np
from skimage.filters import gaussian
import sys


def plot_free(pi, locs_list, ax):
    try:
        _ = ax.properties()['thetamax']
    except KeyError:  # cartesian plot
        x_y = np.array([[np.real(loc), np.imag(loc)] for loc in locs_list])
        x_range = (np.min(x_y[:, 0]), np.max(x_y[:, 0]))
        y_range = (np.min(x_y[:, 1]), np.max(x_y[:, 1]))
        hist, bins2d = np.histogramdd(x_y, bins=(20, 20), range=(x_range, y_range), weights=pi)
        X, Y = np.meshgrid(*bins2d)
    else:  # radial plot
        x_y = np.array([[np.angle(loc), np.abs(loc)] for loc in locs_list])
        max_r = np.max(x_y[:, 1])
        hist, bins2d = np.histogramdd(x_y, bins=(20, 20), range=((-np.pi, np.pi), (0, 1.05*max_r)), weights=pi)
        X, Y = np.meshgrid(np.linspace(-np.pi, np.pi, 20), np.linspace(0, 1.05*max_r, 20))
    hist = gaussian(hist, 0.05)
    free = -0.596*np.log(hist.T+0.0000001)
    offset = np.min(free)
    qq = ax.contourf(X, Y, free - offset)
    plt.colorbar(qq, ax=ax)
        
    
def plot_vector(transmat, pi, locs_list, ax, size=(20, 20)):
    vec_field = np.zeros((*size, 2))
    nstates = transmat.shape[0]
    try:
        _ = ax.properties()['thetamax']
    except KeyError:  # cartesian plot
        x_y = np.array([[np.real(loc), np.imag(loc)] for loc in locs_list])
        x_range = (np.min(x_y[:, 0]), np.max(x_y[:, 0]))
        y_range = (np.min(x_y[:, 1]), np.max(x_y[:, 1]))
        X, Y = np.meshgrid(np.linspace(x_range[0], x_range[1], size[0]),
                           np.linspace(y_range[0], y_range[1], size[1]))
    else:
        x_y = np.array([[np.angle(loc), np.abs(loc)] for loc in locs_list])
        max_r = np.max(x_y[:, 1])
        X, Y = np.meshgrid(np.linspace(-np.pi, np.pi, 20), np.linspace(0, 1.05*max_r, 20))
    x_binedges = np.array(list(X[0, :]) + [X[0, -1] + (X[0, 1] - X[0, 0])]) - 0.5*(X[0, 1] - X[0, 0])
    y_binedges = np.array(list(Y[:, 0]) + [Y[-1, 0] + (Y[1, 0] - Y[0, 0])]) - 0.5 * (Y[1, 0] - Y[0, 0])
    for n_source in range(nstates):
        pop_source = pi[n_source]
        hist_loc_x = np.searchsorted(x_binedges, x_y[n_source][0]) - 1
        hist_loc_y = np.searchsorted(y_binedges, x_y[n_source][1]) - 1
        for n_target in range(nstates):
            vec_source_to_target = vec_diff(x_y[n_source], x_y[n_target])
            flux = pop_source * vec_source_to_target
            vec_field[hist_loc_x, hist_loc_y, :] += flux
    ax.quiver(X, Y, gaussian(vec_field[:, :, 0], 0.75), gaussian(vec_field[:, :, 1], 0.75))
    

def draw_traj(locs_list, model, traj_length, ax, joined=True):
    discrete_traj = model.simulate(traj_length)
    locs_traj = [locs_list[i] for i in discrete_traj]
    try:
        _ = ax.properties()['thetamax']
    except KeyError:  # cartesian plot
        traj = np.array([(np.real(x), np.imag(x)) for x in locs_traj])
    else:
        traj = np.array([(np.angle(x), np.abs(x)) for x in locs_traj])
    colors = plt.cm.coolwarm(np.linspace(0., 1., len(traj)))
    if joined:
        for segm in range(len(traj)-1):
            ax.plot(traj[segm:segm+1, 0], traj[segm:segm+1, 1], c=colors[segm])
    else:
        ax.scatter(traj[:, 0], traj[:, 1], c=colors, s=2)
        

def vec_diff(v1, v2):
    return np.array([v2[0] - v1[0], v2[1] - v1[1]])


if __name__ == "__main__":
    thr, lag = sys.argv[1:]
    m = pyemma.load('model_lag{}.memm'.format(lag))
    locs = pck.load(open('../locs.pck', 'rb'))
    assigns = pck.load(open('renumbered_states_lag{}.pck'.format(lag), 'rb'))
    eq = m.models[-1]
    
    locs_lt = [locs[assigns.inv_states_dict[eq.active_set[ms]]] for ms in range(len(eq.active_set))]
    pi_lt = [eq.pi[eq.active_set[ms]] for ms in range(len(eq.active_set))]
    axis = plt.subplot(111, polar=True)
    axis.set_xticks(np.pi * np.linspace(0, 5/3, 6))
    #axis = plt.subplot(111)
    plot_free(pi_lt, locs_lt, axis)
    # plot_vector(eq.transition_matrix, pi_lt, locs_lt, ax)
    draw_traj(locs_lt, eq, 500, axis)
    plt.savefig('pmf_lag{}_thr{}.svg'.format(lag, thr))

