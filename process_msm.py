from .discretize import sorter
import pickle, os
import numpy as np
from multiprocessing import Pool

direct = '/mnt/storage1/WORK/msm_trajs/TRF1_telo/equil'  # directory in which the analysis is performed


def build_msm(threshold, lagtime):
    """
    Builds MSMs and ensures that eigendecomposition
    was performed in the model, then saves as .pck
    :param threshold:
    :param lagtime:
    :return:
    """
    from msmbuilder.msm import MarkovStateModel
    d = [x for x in os.listdir(direct) if 'contacts' in x]
    d.sort(key=sorter)
    d = [direct + '/' + x for x in d]
    trajs = []
    for x in range(len(d)):
        traj = np.loadtxt(d[x].replace('contacts', 'threshold_{}/clustered-states'.format(threshold)))
        trajs.append(traj)
    msm = MarkovStateModel(lag_time=lagtime)
    msm.fit(trajs)
    _ = msm.timescales_  # ensures that we have eigenv's calculated
    with open(direct + "/msm_lag{}.pck".format(lagtime), 'wb') as wfile:
        pickle.dump(msm, wfile)


def get_implied(lagtime):
    """
    Writes implied timescales for the MSM
    :param lagtime:
    :return:
    """
    with open(direct + "/msm_lag{}.pck".format(lagtime), 'rb') as rfile:
        msm = pickle.load(rfile)
    with open(direct + "/msm_lag{}.tscales".format(lagtime), 'w') as wfile:
        wfile.write(" ".join([str(x) for x in msm.timescales_]))
    # with open(direct + "/msm_lag{}_tscales.pck".format(lagtime), 'wb') as wfile:
    #     pickle.dump(msm, wfile)


def cross_val(lag_thres, prop=0.75):
    """
    Performs cross-validation based on splitting the trajectory set
    into training and testing set with proportion given by prop
    :param lag_thres:
    :param prop:
    :return:
    """
    lagtime = lag_thres[0]
    threshold = lag_thres[1]
    from msmbuilder.msm import MarkovStateModel
    d = [x for x in os.listdir(direct) if 'contacts' in x]
    d.sort(key=sorter)
    d = [direct + '/' + x for x in d]
    d_train = list(np.random.choice(d, int(len(d) * prop), replace=False))
    d_test = [tr for tr in d if tr not in d_train]
    trajs_train = []
    for x in range(len(d_train)):
        traj = np.loadtxt(d_train[x].replace('contacts', 'threshold_{}/clustered-states'.format('threshold')))[::2]
        trajs_train.append(traj)
    msm = MarkovStateModel(lag_time=int(lagtime / 2))
    msm.fit(trajs_train)
    score_train = msm.score(trajs_train)
    trajs_test = []
    for x in range(len(d_test)):
        traj = np.loadtxt(d_test[x].replace('contacts', 'threshold_{}/clustered-states'.format('threshold')))
        trajs_test.append(traj)
    score_test = msm.score(trajs_test)
    with open(direct + '/threshold_{}/scores_lag{}.dat'.format(threshold, lagtime), 'a') as logfile:
        logfile.write("train GMRQ score: {}\n".format(score_train))
        logfile.write("test GMRQ score: {}\n".format(score_test))


def parallel_cv(lagtimes, threshold, nproc=4):
    """
    Wrapper to run cross-validation in parallel
    :param lagtimes:
    :param threshold:
    :param nproc:
    :return:
    """
    p = Pool(nproc)
    p.map(cross_val, [(l, threshold) for l in lagtimes])


def gen_traj(msmfile, length=10000000, ntraj=10):
    """
    Generates MSM-based random trajectories
    of specified length
    :param msmfile:
    :param length:
    :param ntraj:
    :return:
    """
    with open(direct + "/locs.pck", 'rb') as locsfile:
        locs = pickle.load(locsfile)
    with open(msmfile, 'rb') as msmfile:
        msm = pickle.load(msmfile)
    for i in range(ntraj):
        traj = msm.sample(None, length)
        traj_complex = np.array([locs[x] for x in traj])
        with open("traj{}.dat".format(i), 'w') as wfile:
            for frame in traj_complex:
                wfile.write("{:10.4f} {:10.4f}\n".format(*frame))